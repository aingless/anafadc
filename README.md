# AnaFADC

TPC analysis library.

## Usage

```c++
UInt_t myA1Signal[NOFCH][TRACELENGTH];
rootFile = new TFile(filename);  // Load TPC MBS ROOT file.
h101 = (TTree *)rootFile->Get("h101");
h101->SetBranchStatus("*", 0); // Don't read unneeded branches.
for (Int_t ch = 0; ch < NOFCH; ch++) {
    TString brrawv = TString::Format("ADC1CH%dRAWv", ch + 1);
    h101->SetBranchStatus(brrawv, 1);
    h101->SetBranchAddress(brrawv, &myA1Signal[ch]);
}
Long64_t nentries = h101->GetEntries();
for (Long64_t ev = 0; ev < nentries; ev++) {  // Iterate over all events.
    AnaFADC *anaCathode = new AnaFADC(TRACELENGTH);
    anaCathode->config.nOfAnodes = 1;  // Set algorithm settings (see anafadc.C).
    anaCathode->config.nOfLogical = 0;
    anaCathode->config.binWidth = 0.05;
    anaCathode->config.maxPeaks = 50;
    anaCathode->config.ePrecision = 0.01;
    anaCathode->config.smooth1T = 0.5;
    anaCathode->config.derivInterval = 3;
    anaCathode->config.extLeft = 0.7;
    anaCathode->config.extRight = 0.0;
    anaCathode->config.rightIntersection = true;
    anaCathode->config.thr = 15.0;
    anaCathode->config.leftPedestal = true;
    anaCathode->config.minDuration = 20.0;
    for (int j = 0; j < USELENGTH; j++)
        anaCathode->trace[1][j] = myA1Signal[ch][j];  // Fill trace.
    anaCathode->Analyze(1);  // Do analysis.
    e1[ch] = anaCathode->Energy;  // Get signal parameters.
    a1[ch] = anaCathode->Amplitude;
    tbeg[ch] = anaCathode->T_begin_f;
    tend[ch] = anaCathode->T_end_f;
    delete anaCathode;
}
rootFile->Close();
```
